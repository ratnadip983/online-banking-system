<html>
<head>
<link rel="stylesheet" type="text/css" href="adminpage.css">
<title>ABC Bank Ltd:Administration</title>
</head>
<body>
<div class="background">
<div class="header">
<p class="pheader">ADMINISTRATOR</p>
</div>
<div class="name">
<p class="pheader"><a href="adminpage.php">Customer Information</a></p>
</div>
<div class="name">
<p class="pheader"><a href="Branchinfo.php">Branch Information</a></p>
</div>
<div class="name">
<p class="pheader"><a href="btransferlist.php">Balance Transfer Information</a></p>
</div>
<div class="name">
<p class="pheader"><a href="loanlist.php">Loan Information</a></p>
</div>
<div class="name">
<p class="pheader"><a href="balanceaddition.html">Add Balance</a></p>
</div>
<div class="name">
<p class="pheader"><a href="addbranch.html">Add New Branch</a></p>
</div>
<div class="name">
<p class="pheader"><a href="deleteaccount.html">Delete Account</a></p>
</div>
<div class="name">
<p class="pheader"><a href="logout.php">Quit</a></p>
</div>
<div class="display">
	<div class="inheader">
		<p class="pheader">Customer List</p>
	</div>
	<div class="data">
<?php
session_start();
if(!$_SESSION["username"])
{
	Header("Location: index.html");
}
	include("Config.php");
// create query
$query = "SELECT * FROM customer,depositor,account WHERE account.Account_id=depositor.Account_id AND depositor.Customer_id=customer.Customer_id";

// execute query
$result = mysql_query($query)or die ("Error in query: $query. ".mysql_error());
// see if any rows were returned
if (mysql_num_rows($result) > 0) {
    // yes
    // print them one after another
    echo "<table align=center border=1>";
		echo "<tr>";
		echo "<th>Name</th>";
		echo "<th>Account ID</th>";
		echo "<th>Balance</th>";
		echo "<th>E-Mail</th>";
		echo "<th>Phone</th>";
		echo "<th>Gender</th>";
		echo "<th>Birth Date</th>";
		echo "</tr>";
    while($row = mysql_fetch_array($result)) {
		$name=$row['First_Name']." ".$row['Middle_Name']." ".$row['Last_Name'];
		echo "<tr>";
       echo "<td>".$name."</td>";
       echo "<td>".$row['Account_id']."</td>";
	   echo "<td>".$row['Current_Balance']."</td>";
	   echo "<td>".$row['E_mail']."</td>";
	   echo "<td>".$row['Phone']."</td>";
	   echo "<td>".$row['Gender']."</td>";
	   echo "<td>".$row['DateOfBirth']."</td>";
        echo "</tr>";
    }
    echo "</table>";
}
else {
    // no
    // print status message
    echo "No Customer Found!";
}

// free result set memory
//mysql_free_result($result); 
	//

?>
</div>
</div>

</div>
</body>
</html>