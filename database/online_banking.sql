-- phpMyAdmin SQL Dump
-- version 2.11.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 24, 2013 at 02:00 PM
-- Server version: 5.0.51
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `online_banking`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `Account_id` varchar(40) NOT NULL,
  `Branch_id` int(15) NOT NULL,
  `Current_Balance` double NOT NULL,
  PRIMARY KEY  (`Account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`Account_id`, `Branch_id`, `Current_Balance`) VALUES
('0010', 3, 0),
('C0002', 2, 1000),
('C0003', 2, 0),
('C0005', 2, 0),
('Cox0013', 1, 1000),
('D0004', 3, 0),
('D0006', 3, 0),
('D0007', 3, 250),
('D0010', 1, 250),
('F0011', 3, 10644),
('L0012', 7, 600),
('N0008', 1, 1100);

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `user_name` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL,
  PRIMARY KEY  (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`user_name`, `password`) VALUES
('arif', '123456789'),
('ratnadip', '4530908');

-- --------------------------------------------------------

--
-- Table structure for table `balance_transfer`
--

CREATE TABLE `balance_transfer` (
  `Transfer_id` int(15) NOT NULL auto_increment,
  `Sender_id` varchar(30) NOT NULL,
  `Recever_id` varchar(30) NOT NULL,
  `Amount` double NOT NULL,
  `Transfer_date` date NOT NULL,
  PRIMARY KEY  (`Transfer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `balance_transfer`
--

INSERT INTO `balance_transfer` (`Transfer_id`, `Sender_id`, `Recever_id`, `Amount`, `Transfer_date`) VALUES
(3, 'F0011', 'N0009', 300, '2013-04-22'),
(4, 'F0011', 'L0012', 500, '2013-04-23'),
(5, 'F0011', 'Cox0013', 1000, '2013-04-23'),
(6, 'F0011', 'L0012', 100, '2013-04-24');

-- --------------------------------------------------------

--
-- Table structure for table `borrow`
--

CREATE TABLE `borrow` (
  `Customer_id` int(15) NOT NULL,
  `Loan_id` int(15) NOT NULL,
  `Borrowing_date` date NOT NULL,
  PRIMARY KEY  (`Customer_id`,`Loan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `borrow`
--

INSERT INTO `borrow` (`Customer_id`, `Loan_id`, `Borrowing_date`) VALUES
(11, 4, '2013-04-22'),
(11, 6, '2013-04-23');

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `Branch_id` int(15) NOT NULL auto_increment,
  `Branch_name` varchar(30) NOT NULL,
  `Branch_city` varchar(30) NOT NULL,
  `Assets` double NOT NULL,
  PRIMARY KEY  (`Branch_id`),
  UNIQUE KEY `Branch_name` (`Branch_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`Branch_id`, `Branch_name`, `Branch_city`, `Assets`) VALUES
(1, 'Noakhali', 'Noakhali', 149650),
(2, 'Comilla', 'Comilla', 150000),
(3, 'Dhaka', 'Dhaka', 198650),
(4, 'Feni', 'Feni', 130000),
(5, 'Chittagong', 'Chittagong', 175000),
(6, 'CoxBazar', 'CoxBazar', 101000),
(7, 'Laxmipur', 'Laxmipur', 100700),
(8, 'Dhanmondi', 'Dhaka', 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `Customer_id` int(10) NOT NULL auto_increment,
  `First_Name` varchar(30) NOT NULL,
  `Middle_Name` varchar(20) default NULL,
  `Last_Name` varchar(40) NOT NULL,
  `Customer_City` varchar(50) NOT NULL,
  `Postal_code` varchar(20) NOT NULL,
  `Road_Number` varchar(30) NOT NULL,
  `House_Number` varchar(30) NOT NULL,
  `E_mail` varchar(50) NOT NULL,
  `Phone` varchar(15) NOT NULL,
  `Gender` varchar(20) NOT NULL,
  `DateOfBirth` date NOT NULL,
  `Password` varchar(50) NOT NULL,
  `create_date` date NOT NULL,
  `update_date` date default NULL,
  PRIMARY KEY  (`Customer_id`),
  UNIQUE KEY `E_mail` (`E_mail`,`Phone`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`Customer_id`, `First_Name`, `Middle_Name`, `Last_Name`, `Customer_City`, `Postal_code`, `Road_Number`, `House_Number`, `E_mail`, `Phone`, `Gender`, `DateOfBirth`, `Password`, `create_date`, `update_date`) VALUES
(2, 'Arifur', '', 'Rahaman', 'Comilla', '3741', '13C', '12D', 'arifbd@gmail.com', '017245302215', 'Male', '1991-03-04', '', '2013-04-20', NULL),
(3, 'Nurul', 'Alam', 'Masud', 'Comilla', '3730', '13C', '14A', 'masudcste@gmail.com', '01813282604', 'Male', '1991-03-04', '', '2013-04-20', NULL),
(4, 'Syful', '', 'Islam', 'Madaripur', '4012', '14D', '15D', 'syfulcste@gmail.com', '01823024498', 'Male', '1990-05-04', '', '2013-04-20', NULL),
(5, 'Ismail', '', 'Hossain', 'Comilla', '4011', '12D', '14A', 'ismailcste@gmail.com', '01823024491', 'Male', '1990-06-03', '', '2013-04-20', NULL),
(6, 'Ismail', '', 'Hossain', 'Comilla', '4011', '12D', '14A', 'ismailcste2@gmail.com', '01823024491', 'Male', '2001-06-03', '', '2013-04-20', NULL),
(7, 'Mrinal', 'Kanti', 'Baowaly', 'Khulna', '3704', '12C', '15D', 'boawaly@gmail.com', '01823024425', 'Male', '1978-06-03', '', '2013-04-20', NULL),
(8, 'Indra', 'Kumar', 'Kuri', 'Noakhali', '3834', '13D', '12D', 'indra55@gmail.com', '018542369', 'Male', '1989-01-01', '4530908', '2013-04-20', NULL),
(10, 'Avro', '', 'Roy', 'Dhaka', '3814', '13B', '25A', 'avro12@gmail.com', '012365489', 'Male', '1996-05-06', '159753', '2013-04-21', '2013-04-21'),
(11, 'Monir', '', 'Mahamud', 'Bbaria', '3833', '12A', '14D', 'monir@gmail.com', '01325478965', 'Male', '1990-05-19', '147852', '2013-04-22', '2013-04-22'),
(12, 'Riad', '', 'Hossain', 'Comilla', '3814', '12A', '25C', 'riad@gmail.com', '0125693874', 'Male', '1990-04-01', '987654321', '2013-04-22', NULL),
(13, 'Abhijit', '', 'Kuri', 'Noakhali', '3834', '2A', '13B', 'abhijit123@gmail.com', '01236589547', 'Male', '1994-06-13', '159357', '2013-04-23', '2013-04-23');

-- --------------------------------------------------------

--
-- Table structure for table `depositor`
--

CREATE TABLE `depositor` (
  `Customer_id` int(10) NOT NULL,
  `Account_id` varchar(15) NOT NULL,
  `Deposite_Date` varchar(15) NOT NULL,
  PRIMARY KEY  (`Customer_id`,`Account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `depositor`
--

INSERT INTO `depositor` (`Customer_id`, `Account_id`, `Deposite_Date`) VALUES
(2, 'C0002', '2013-04-24'),
(3, 'C0003', ''),
(4, 'D0004', ''),
(5, 'C0005', ''),
(6, 'D0006', ''),
(7, 'D0007', ''),
(8, 'N0008', '2013-04-24'),
(10, 'D0010', ''),
(11, 'F0011', ''),
(12, 'L0012', ''),
(13, 'Cox0013', '');

-- --------------------------------------------------------

--
-- Table structure for table `loan`
--

CREATE TABLE `loan` (
  `Loan_id` int(11) NOT NULL auto_increment,
  `Branch_id` int(11) NOT NULL,
  `Amount` double NOT NULL,
  PRIMARY KEY  (`Loan_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `loan`
--

INSERT INTO `loan` (`Loan_id`, `Branch_id`, `Amount`) VALUES
(1, 1, 200),
(4, 3, 400),
(6, 5, 700);
